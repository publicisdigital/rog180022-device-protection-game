var express = require('express'),
    http    = require('http'),
    app     = express(),
    server  = http.createServer(app);
    port    = process.env.PORT || 8080;

var player1_sockets = {};
var player2_sockets = {};

server.listen(port);

// Set up index
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.use('/socket.io', express.static(__dirname + '/node_modules/socket.io-client/dist'));
app.use('/img', express.static(__dirname + '/img'));
app.use('/audio', express.static(__dirname + '/audio'));
app.use('/js', express.static(__dirname + '/js'));


// Log that the servers running
console.log("Server running on port: " + port);

var io = require('socket.io').listen(server);

io.sockets.on('connection', function (socket) {
    socket.on('player1_connect', function(){
        console.log("Player1 connected");
        player1_sockets[socket.id] = {
            socket: socket,
            player2_id: undefined
        };
        socket.emit("player1_connected");
    });
    socket.on('player2_connect', function(player1_socket_id){
        if (player1_sockets[player1_socket_id] && !player1_sockets[player1_socket_id].player2_id) {
            console.log("Player2 connected");
            player2_sockets[socket.id] = {
                socket: socket,
                player1_id: player1_socket_id
            };
            player1_sockets[player1_socket_id].player2_id = socket.id;
            player1_sockets[player1_socket_id].socket.emit("player2_connected", true);
            socket.emit("player2_connected", true);
        } else {
            console.log("Player2 attempted to connect but failed");
            socket.emit("player2_connected", false);
        }
    });
    socket.on('disconnect', function() {
        // Player1
        if (player1_sockets[socket.id]) {
            console.log("Player1 disconnected");
            if (player2_sockets[player1_sockets[socket.id].player2_id]) {
                player2_sockets[player1_sockets[socket.id].player2_id].socket.emit("player2_connected", false);
                player2_sockets[player1_sockets[socket.id].player2_id].player1_id = undefined;
            }
            delete player1_sockets[socket.id];
        }
        // Player2
        if (player2_sockets[socket.id]) {
            console.log("Player2 disconnected");
            if (player1_sockets[player2_sockets[socket.id].player1_id]) {
                player1_sockets[player2_sockets[socket.id].player1_id].socket.emit("player2_connected", false);
                player1_sockets[player2_sockets[socket.id].player1_id].player2_id = undefined;
            }
            delete player2_sockets[socket.id];
        }
    });
    socket.on('player_state_change', function(player_state){
        if (player_state.throw == true) {
            console.log(player_state.id);
            // io.sockets.emit("throwSync", player_state);
            if (player1_sockets[player_state.id]){
                player2_sockets[player1_sockets[player_state.id].player2_id].socket.emit("throwSync", player_state);
            }
            if (player2_sockets[player_state.id]) {
                player1_sockets[player2_sockets[player_state.id].player1_id].socket.emit("throwSync", player_state);
            }
        }
    });
});
